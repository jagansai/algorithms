# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : The list of algorithms, datastructures & code related questions that are frequently asked in interviews.
* Version : 1.0

### How do I get set up? ###

* Summary of set up :   
         clone the repository in your environment.
		 
* Configuration :   
         clone range-v3 library and set the environment variable HEADER_FILE_PATH=/path_to_range_library  

* Dependencies :   
         range-v3 library
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact