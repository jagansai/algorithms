// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#ifndef STDAFX_H
#define STDAFX_H

#define Stringize( L )     #L 
#define MakeString( M, L ) M(L)
#define $Line MakeString( Stringize, __LINE__ )
#define Reminder __FILE__ "(" $Line ") : Reminder: "


#include <stdio.h>
//#include <tchar.h>
#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <forward_list>
#include <list>

#include <unordered_set>
#include <algorithm>
#include <iterator>
#include <utility>
#include <random>
#include <fstream>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <future>
#include <queue>
#include <chrono>
#include <memory>
#include <iterator>
#include <type_traits>
#include <string>
#include <sstream>
//#include "fmt/format.h"


#endif


// TODO: reference additional headers your program requires here
