#ifndef TREE_H
#define TREE_H

#include "../stdafx.h"
#include <memory>


template<typename Type, typename Compare = std::less<Type>>
class Tree
{

private:
    struct Node
    {
        Type m_data;
        typedef std::unique_ptr<Node> NodePtr;


        NodePtr m_left;
        NodePtr m_right;
        Node* m_sibling;

        Node(Type const& data)
            : m_data(data)
            , m_sibling(nullptr)
        {
        }

        static auto make_node(Type const& data)
        {
            return std::make_unique<Node>(data);
        }
    };

private:
    std::unique_ptr<Node> m_head;
    Compare m_compare;

    typedef std::unique_ptr<Node> node_ptr;
    typedef const std::unique_ptr<Node> const_node_ptr;

public:
    Tree(Type const& data, Compare compare)
        : m_head(data)
        , m_compare(compare)
    {
    }

    Tree()
        : m_compare()
    {
    }

    explicit Tree(Compare compare)
        : m_compare(compare)
    {
    }

    Tree(Tree&& other) noexcept
        : m_head(std::move(other.m_head))
        , m_compare(other.m_compare)
    {
    }

    Tree& operator=(Tree&& other) noexcept
    {
        if (this != &other)
        {
            m_head = std::move(other.m_head);
            m_compare = other.m_compare;
        }
        return *this;
    }


    Tree(Tree const& other)
        : m_compare(other.m_compare)
    {
        Clone(other);
    }

    Tree& operator=(const Tree& other)
    {
        if (this != &other)
        {
            m_compare = other.m_compare;
            Clone(other);
        }
        return *this;
    }


    void Add(Type const& data)
    {
        if (!m_head)
        {
            m_head = std::make_unique<Node>(data);
        }
        else
        {
            Insert(m_head, data);
        }
    }

    bool Exists(Type const& data)
    {
        if (!m_head)
            return false;
        return SearchForData(m_head, data);
    }

    void Delete(Type const& data)
    {
        if (!m_head)
            return;
        return DeleteImpl(m_head, m_head, data);
    }

    size_t depth() const
    {
        return depth(m_head);
    }

    std::ostream& Print(std::ostream& os = std::cout)
    {
        Print(m_head, os);
        return os;
    }

    friend std::ostream& operator<<(std::ostream& os, Tree const& t)
    {
        t.Print(os);
        return os;
    }


private:
    size_t depth(const node_ptr& node) const
    {
        if (node)
            return 1 + std::max(depth(node->m_left), depth(node->m_right));
        return 0;
    }

    void DeleteImpl(node_ptr& curr_node, node_ptr& parent, Type const& data)
    {
        if (!curr_node)
            return;

        if (m_compare(curr_node->m_data, data))
        {
            DeleteImpl(curr_node->m_right, curr_node, data);
        }
        else if (m_compare(data, curr_node->m_data))
        {
            DeleteImpl(curr_node->m_left, curr_node, data);
        }
        else if (!curr_node->m_left && !curr_node->m_right)
        {
            if (!parent) // null
            {
                curr_node.reset(nullptr);
                m_head.reset(nullptr);
            }
            else if (parent->m_left.get() == curr_node.get())
            {
                parent->m_left.reset(nullptr);
            }
            else if (parent->m_right.get() == curr_node.get())
            {
                parent->m_right.reset(nullptr);
            }
        }
        else if (curr_node->m_right)
        {
            Type temp = curr_node->m_data;
            curr_node->m_data = curr_node->m_right->m_data;
            curr_node->m_right->m_data = temp;
            DeleteImpl(curr_node->m_right, curr_node, data);
        }
        else if (curr_node->m_left)
        {
            Type temp = curr_node->m_data;
            curr_node->m_data = curr_node->m_left->m_data;
            curr_node->m_left->m_data = temp;
            DeleteImpl(curr_node->m_left, curr_node, data);
        }
    }

    
    bool SearchForData(node_ptr& node, Type const& data)
    {
        if (!node)
            return false;
        const bool not_less = !m_compare(data, node->m_data);
        const bool not_greater = !m_compare(node->m_data, data);
        if (not_less && not_greater)
            return true;
        if (not_greater)
            return SearchForData(node->m_left, data);
        if (not_less)
            return SearchForData(node->m_right, data);
        else
            return false; // cannot happen.
    }

    void Insert(node_ptr& node, Type const& data)
    {
        if (!node)
        {
            node = std::make_unique<Node>(data);
            return;
        }
        if (m_compare(data, node->m_data))
        {
            Insert(node->m_left, data);
        }
        else
        {
            Insert(node->m_right, data);
        }
    }

    void Clone(Tree const& other)
    {
        if (!other.m_head)
            return;

        m_head = Node::make_node(other.m_head->m_data);
        Clone(m_head, other.m_head);
    }

    void Clone(node_ptr& node, const_node_ptr& otherNode)
    {
        if (!otherNode)
            return;
        node = Node::make_node(otherNode->m_data);
        Clone(node->m_left, otherNode->m_left);
        Clone(node->m_right, otherNode->m_right);
    }


    void Print(const_node_ptr& node, std::ostream& os)
    {
        if (node)
        {
            Print(node->m_left, os);
            os << node->m_data << '\n';
            Print(node->m_right, os);
        }
    }

    void preorder(const node_ptr& root) {
        if (root) {
            std::cout << root->m_data << ' ';
            preorder(root->m_left);
            preorder(root->m_right);
        }
    }

    void posorder(const node_ptr& root) {
        if (root) {
            posorder(root->m_left);
            posorder(root->m_left);
            std::cout << root->m_data << ' ';
        }
    }

};


struct Employee
{
    std::string name;
    double salary;


    Employee(std::string name, double salary)
        : name(std::move(name))
        , salary(salary)
    {

    }

    Employee(Employee&&) = default;
    Employee(Employee const&) = default;
    Employee& operator=(Employee&&) = default;
    Employee& operator=(Employee const&) = default;


    static bool ByName(Employee const& lhs, Employee const& rhs)
    {
        return lhs.name < rhs.name;
    }

    static bool BySal(Employee const& lhs, Employee const& rhs)
    {
        return lhs.salary < rhs.salary;
    }


    friend std::ostream& operator<<(std::ostream& os, Employee const& other)
    {
        os << other.name << "," << other.salary;
        return os;
    }

};

void TreeDemo()
{
    {
        Tree<int> tree;
        tree.Add(5);
        tree.Add(3);
        tree.Add(4);
        tree.Add(2);
        tree.Add(2);
        tree.Add(6);
        tree.Add(7);
        std::cout << "Printing contents of the tree\n";
        tree.Print();

    }
    {
        typedef bool(*Comparator)(Employee const&, Employee const&);

        Tree<Employee, Comparator> tree(Employee::BySal);
        tree.Add(Employee{ "Jagan", 105 });
        tree.Add(Employee{ "Nath", 103 });
        tree.Add(Employee{ "Pantula", 104 });
        tree.Add(Employee{ "Sai", 107 });

        std::cout << "Printing contents of the tree\n";
        tree.Print();
    }

    {
        typedef bool(*Comparator)(Employee const&, Employee const&);

        Tree<Employee, Comparator> tree(Employee::ByName);
        tree.Add(Employee{ "Jagan", 105 });
        tree.Add(Employee{ "Nath", 103 });
        tree.Add(Employee{ "Pantula", 104 });
        tree.Add(Employee{ "Sai", 107 });

        std::cout << "Printing contents of the tree\n";
        tree.Print();
    }
    {
        Tree<int> tree;
        tree.Add(5);
        tree.Add(4);
        tree.Add(3);
        tree.Add(7);
        tree.Add(6);

        Tree<int> anotherTree = tree;
        std::cout << "Copy constructing a tree from other tree\n";
        anotherTree.Print();
        std::cout << "searching for " << 6 << "returns ";
        std::cout << std::boolalpha << anotherTree.Exists(6) << '\n';


        std::cout << "Printing tree before deletion of 7\n";
        tree.Print();


        tree.Delete(7);
        std::cout << "Printing tree after deleting 7\n";
        tree.Print();
    }
    {
        {

            std::cout << "Demo of copy assignment of a tree\n";
            Tree<int> tree;
            tree.Add(5);
            tree.Add(4);
            tree.Add(3);
            tree.Add(7);
            tree.Add(6);

            std::cout << "Printing contents of tree\n";
            tree.Print();

            Tree<int> anotherTree;
            anotherTree.Add(10);
            anotherTree.Add(8);
            anotherTree.Add(11);

            std::cout << "Printing contents of tree\n";
            anotherTree.Print();

            std::cout << "Assigning the first tree to second.\n";

            anotherTree = tree;
            anotherTree.Print();
            std::cout << "depth of the tree:" << anotherTree.depth() << '\n';
        }
    }
}

#endif