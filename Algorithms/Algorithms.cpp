// Algorithms.cpp : Defines the entry point for the console application.
//

#include "../stdafx.h"
#include "MyUtility.h"
#include "RemoveDuplicates.h"
#include "GetUniqueItems.h"
#include "Tree.h"
#include "ProducerConsumer.h"
#include "List.h"
#include "TestJSON.h"
#include "RefCountedSafePtr.h"
#include "RefCountedStr.h"
#include "rule_of_three.h"
#include "rule_of_five.h"
#include "rule_of_zero.h"

#include "CRTPUseCase.h"
#include "PrettyPrinter.h"
#include "Visitor.h"
#include "AutoTest.h"
#include "NamedTypes.h"
#include "RangeDemo.h"
#include "ParTestsDemo.h"




void test_reverse_iter()
{
	std::vector<int> v{ 1,2,3,4 };
	for (auto e : Util::reverse(v))
	{
		std::cout << e << '\n';
	}
}


int main(int argc, char* argv[])
{

	TreeDemo();
	test_rule_of_three();
	test_rule_of_five();
	test_rule_of_zero();
	test_reverse_iter();

	{
		auto print = [](int x, int y) { std::cout << '(' << x << ',' << y << ")\n"; };
		std::vector<int> v{ 1,2,3,4,5 };
		std::cout << "adjacent_paris...\n";
		Util::Algorithms::adjacent_pair(v.begin(), std::next(v.begin(), 3), print);
		std::cout << "for_allparis...\n";

		Util::Algorithms::for_all_pairs(v.begin(), v.end(), print);
	}
	{
		std::future<MyStaticData&> ft1 = std::async([]() -> MyStaticData&
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(200));
			std::cout << "getting static data now...\n";
			return MyStaticData::makeInstance_static();
		});
		MyStaticData& mystaticData = MyStaticData::makeInstance_static();
		std::cout << mystaticData.data() << '\n';

		std::cout << ft1.get().data() << '\n';
	}
	{
		// Get all the elements in a vector.
		std::vector<int> v{ 1, 1, 2, 2, 3,3,3, 4, 5,7 };
		for (auto e : Util::Algorithms::getAllElements(v.begin(), v.end(), 3))
		{
			std::cout << e << '\n';
		}
	}
	{
		// Get all the elements in a list.
		std::list<int> v{ 1, 1, 2, 2, 3,3,3, 4, 5,7 };
		for (auto e : Util::Algorithms::getAllElements(v.begin(), v.end(), 3))
		{
			std::cout << e << '\n';
		}
	}

	auto GetNameAndAge = []()
	{
		return std::make_tuple(std::string("jagan"), 38);
	};

	auto [name,_] = GetNameAndAge();
	std::cout << name << '\n';

    PrettyPrintDemo();
    NameTypeDemo();

//    VisitorDemo();

    AutoTest();
	RangeDemo();
    ParallelSTLDemo();

	std::cout << "Press Enter to finish";
	std::cin.ignore();

    return 0;
}

