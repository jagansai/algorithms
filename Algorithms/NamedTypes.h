#pragma once

#include <string>
#include <ostream>
#include <iostream>

namespace SafeType {

    template<typename Tag>
    struct IntType {
    private:
        int val_;
    public:
        IntType(int val) : val_{val} 
        {}
        int& get() {
            return val_;
        }
        int get() const {
            return val_;
        }

        void set(int val) {
            val_ = val;
        }
    };
}


using Age = SafeType::IntType<struct AgeTag>;
using Sal = SafeType::IntType<struct SalTag>;

struct Person {
    std::string name_;
    Age age_;
    Sal sal_;

public:
    Person(std::string name, Age age, Sal sal)
        : name_(name)
        , age_(age)
        , sal_(sal)
    {}

    friend std::ostream& operator<<(std::ostream& os, Person const& person) {
        os << person.name_ << ',' << person.age_.get() << ',' << person.sal_.get() << '\n';
        return os;
    }
};

void NameTypeDemo() {
    Person jagan{ "jagan", Age{39}, Sal{10000} };
    std::cout << jagan << '\n';
}