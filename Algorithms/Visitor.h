#pragma once

#include <string>
#include <utility>
#include <variant>
#include <iostream>
#include <string_view>
//#include <fmt/format.h>

template<typename... Base>
struct Visitor : Base... {
    using Base::operator()...;
};

template<typename...T>
Visitor(T...)->Visitor < T... >;


void VisitorDemo()
{
    /*Visitor visitor{
            [](int i) { std::cout << fmt::format("int is passed. Value is {}", i) << '\n'; },
            [](std::string_view name) { std::cout << fmt::format("string is passed. Value is {}", name) << '\n'; }
    };

    constexpr auto v = std::variant<int, std::string_view>{ std::string_view{"jagan"} };
    std::visit(visitor, v);
    std::visit(visitor, std::variant<int, std::string_view>{10});*/
}