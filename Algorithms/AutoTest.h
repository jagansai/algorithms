#pragma once
#include <string>

auto func1()
{
    static std::string value{ "value" };
    auto& result = value;
    return result;
}

auto& func2()
{
    static std::string value{ "value" };
    auto& result = value;
    return result;
}

decltype(auto) func3()
{
    static std::string value{ "value" };
    auto result = value;
    return std::move(result);
}


void AutoTest()
{
    static_assert(std::is_same_v<decltype(func1()), std::string>, "Doesn't return string");
    static_assert(std::is_same_v<decltype(func2()), std::string&>, "Doesn't return string&");
    static_assert(std::is_same_v<decltype(func3()), std::string&&>, "Doesn't return string&&");
}