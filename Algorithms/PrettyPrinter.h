//
// PrettyPrinter.h: Helps to print various containers in formatted way.
// For e.g.,
// std::cout << std::vector<int>{1,2,3,4,5} << '\n'; will output [1, 2, 3, 4, 5]
// std::cout << std::cout << std::map<int, std::string>{{1,"one"}, {2, "two"}}
// << '\n'; will output [1 -> "one, 2 -> "two" ]
//

#include <deque>
#include <forward_list>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <numeric>
#include <set>
#include <sstream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <vector>
#include <utility>

// Generate Print method for various containers.
// std::vector, std::list, ...
#define GENERATE_CONTAINER_METHODS(CONT)                                       \
  template <typename T> std::string Print(CONT<T> const &data) {               \
    return PrettyPrintImpl(data.cbegin(), data.cend(),                         \
                           typename std::iterator_traits<decltype(             \
                               data.cbegin())>::iterator_category());          \
  }

// Generate Print methods for various associative containers.
// std::map, std::unordered_map ...
#define GENERATE_ASSOCIATE(CONT)                                               \
  template <typename K, typename V>                                            \
  std::string Print(CONT<K, V> const &data) {                                  \
    return PrettyPrintImpl(data.begin(), data.end());                          \
  }

namespace PrettyPrint {

  /*  template<typename Iter, typename IterTag>
class PrettyPrinter {
    
};

template<typename Iter>
class PrettyPrinter<Iter, std::forward_iterator_tag> {

public:
    PrettyPrinter(Iter start, Iter finish)
    : start_(start)
    , finish_(finish){}

    std::string Print() {
        std::ostringstream oss;
        oss << '[';
        for (; start_ != finish_; ++start_) {
            oss << *start_ << ", ";
        }
        std::string result = oss.str();
        return result.substr(0, result.size() - 2) + ']';
    }

private:
    Iter start_;
    Iter finish_;

};

template<typename Iter>
class PrettyPrinter<Iter, std::enable_if_t<std::is_same_v<
    typename std::iterator_traits<Iter>::value_type,
    std::pair < decltype(std::declval<Iter>()->first), decltype(std::declval<Iter>()->second) >> > * > {
public:
    PrettyPrinter(Iter start, Iter finish)
        : start_(start)
        , finish_(finish) {}

    std::string Print() {
        std::ostringstream oss;
        oss << '[';
        for (; start_ != finish_; ++start_) {
            oss << start_->first << " -> " << start_->second << ", ";
        }
        std::string result = oss.str();
        return result.substr(0, result.size() - 2) + ']';
    }

private:
    Iter start_;
    Iter finish_
};*/

template <typename Iterator>
std::string PrettyPrintImpl(Iterator first, Iterator last,
                            std::forward_iterator_tag) {
  std::ostringstream oss;
  oss << '[';
  for (; first != last; ++first) {
    oss << *first << ", ";
  }
  std::string result = oss.str();
  return result.substr(0, result.size() - 2) + ']';
}

template <typename Iterator>
std::string PrettyPrintImpl(
    Iterator start, Iterator finish,
    std::enable_if_t<std::is_same_v<
        typename std::iterator_traits<Iterator>::value_type,
        std::pair<decltype(start->first), decltype(start->second)>>> * = 0) {
  std::ostringstream oss;
  oss << '[';
  for (; start != finish; ++start) {
    oss << start->first << " -> " << start->second << ", ";
  }
  std::string result = oss.str();
  return result.substr(0, result.size() - 2) + ']';
}

GENERATE_CONTAINER_METHODS(std::vector)
GENERATE_CONTAINER_METHODS(std::list)
GENERATE_CONTAINER_METHODS(std::forward_list)
GENERATE_CONTAINER_METHODS(std::deque)
GENERATE_CONTAINER_METHODS(std::set)

GENERATE_ASSOCIATE(std::map)
GENERATE_ASSOCIATE(std::unordered_map)
} // namespace PrettyPrint

template <typename Type> std::vector<Type> GetData(const int num) {
  std::vector<Type> result(num);
  std::iota(result.begin(), result.end(), 1);
  return result;
}

void PrettyPrintDemo() {
  std::cout << PrettyPrint::Print(GetData<int>(1000)) << '\n';
  std::cout << PrettyPrint::Print(std::list<int>{1, 2, 3, 4, 5}) << '\n';

  std::cout << PrettyPrint::Print(std::set<int>{1, 2, 3, 4, 5}) << '\n';

  std::cout << PrettyPrint::Print(
                   std::map<int, std::string>{{1, "one"}, {2, "two"}})
            << '\n';
  std::cout << PrettyPrint::Print(std::unordered_map<int, std::string>{
                   {4, "four"}, {5, "five"}})
            << '\n';
}