//
// Created by jagan on 08-11-2018.
//

#ifndef ALGORITHMS_RANGEDEMO_H
#define ALGORITHMS_RANGEDEMO_H

#include "range/v3/all.hpp"
#include <string>
#include <iostream>

using namespace ranges;

void RangeDemo() {
    std::cout << __FUNCTION__ << '\n';

   // std::vector<int> v {1,2,3,4,5,6,7};
    auto gt_x = [](auto elem_to_compare) {
        return [=](auto elem) {
            return elem > elem_to_compare;
        };
    };

    auto isEven = [](int x) { return x % 2 == 0; };
    auto multiplyBy = [](int x)
    {
        return [x](int y)
        {
            return x * y;
        };
    };

    auto to_string = [](auto x)
    {
        return std::to_string(x);
    };

    auto rng = view::ints(1) | view::take(10) 
                             | view::remove_if(isEven) 
                             | view::transform(multiplyBy(3));
    for (auto &&r : rng) {
        std::cout << r << '\n';
    }

}


#endif //ALGORITHMS_RANGEDEMO_H
