#pragma once
#include "MyUtility.h"
#include "../stdafx.h"
#include <algorithm>
#include <execution>

void ParallelSTLDemo( )
{
    std::vector<double> v(4'500'007, 0.5);
    {
        auto result = CPPUtils::TimeTaken([&]( ) {
            return std::reduce(std::execution::seq, v.begin( ), v.end( ), 0.0);
        }, "std::warmup", std::cout);
        std::cout << result << '\n';
    }
    {
        auto result = CPPUtils::TimeTaken([&]( ) {
            return std::reduce(std::execution::seq, v.begin( ), v.end( ), 0.0);
        }, "std::reduce, seq", std::cout);
        std::cout << result << '\n';

    }

    {
        auto result = CPPUtils::TimeTaken([&]( ) {
            return std::reduce(std::execution::par, v.begin( ), v.end( ), 0.0);
        }, "std::reduce, par", std::cout);
        std::cout << result << '\n';

    }
}
