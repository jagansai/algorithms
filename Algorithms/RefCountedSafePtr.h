#pragma once
#include "../stdafx.h"
#include "RefCounter.h"



struct Deleter
{
    template<typename T>
    void operator()(const T* ptr) const
    {
        // std::cout << "Deleting " << *ptr << '\n';
        delete ptr;
    }
};

template<typename T, typename deleter = Deleter>
class RefCountedSafePtr
{
    RefCounter *m_refCounter;
    T* m_ptr;

public:
    explicit RefCountedSafePtr(T* ptr = nullptr)
        : m_ptr(nullptr)
    {
        if (ptr)
        {
            m_ptr = ptr;
            m_refCounter = new RefCounter();
        }
    }

    RefCountedSafePtr(const RefCountedSafePtr& other)
    {
        if (other.m_ptr)
        {
            m_ptr = other.m_ptr;
            m_refCounter = other.m_refCounter;
        }
    }

    RefCountedSafePtr& operator=(const RefCountedSafePtr& other)
    {
        using std::swap;
        if (this != &other)
        {
            if (m_refCounter)
            {
                RefCountedSafePtr<T> temp{ other };
                swap(this->m_ptr, temp.m_ptr);
                swap(this->m_refCounter, temp.m_refCounter);
            }
        }
        return *this;
    }

    T* get()
    {
        return m_ptr;
    }

    T& operator*()
    {
        return *m_ptr;
    }

    void reset(T* ptr = nullptr)
    {
        using std::swap;
        RefCountedSafePtr<T> temp{ ptr };
        swap(this->m_ptr, temp.m_ptr);
        swap(this->m_refCounter, temp.m_refCounter);
    }


    ~RefCountedSafePtr() noexcept
    {
        cleanup();
    }

private:
    void cleanup() noexcept
    {
        m_refCounter->decrement();
        if (m_refCounter->refCount() == 0)
        {
            deleter()(m_ptr);
            delete m_refCounter;
        }
    }
};