#pragma once

namespace Algorithms {

    std::vector<std::pair<int, int>> FindPairsOfSumN(std::vector<int> const& collection, int sum) {
        std::vector<std::pair<int, int>> pairs;
        std::unordered_set<int> values;
        for (auto& e : collection) {
            auto target = sum - e;
            if (values.find(target) != values.end()) {
                pairs.emplace_back(std::make_pair(e, target));
            }
            else {
                values.insert(e);
            }
        }
        return pairs;
    }
}